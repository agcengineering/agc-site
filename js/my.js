
      // Gets language preference on page load
      $(document).ready(function () {
        // --- Acquires language passed through launcher app --- //
        var urlParams = new URLSearchParams(window.location.search);
        var launcherLang = urlParams.get('lang');
        console.log('Launcher language : ' + urlParams.get('lang'));
        // --- Acquires language passed through cookies --- //
        var cookiesLang = localStorage.getItem('user_lang');
        console.log('Cookies language: ' + cookiesLang);
        // --- Acquires browser default language --- //
        var browserLang = (
          navigator.language || navigator.userLanguage
        ).substring(0, 2);
        console.log('Browser language: ' + browserLang);

        var finalLang = null;
        if (launcherLang) {
          finalLang = launcherLang;
        } else if (cookiesLang) {
          finalLang = cookiesLang;
        } else if (browserLang) {
          finalLang = browserLang;
        }

        console.log("Site's final language will be: " + finalLang);

        switch (finalLang) {
          case 'en':
            $.MultiLanguage('language.json', 'en');
            break;
          case 'pl':
            $.MultiLanguage('language.json', 'pl');
            break;
          case 'el':
            $.MultiLanguage('language.json', 'el');
            break;
          case 'ro':
            $.MultiLanguage('language.json', 'ro');
            break;
          default:
            $.MultiLanguage('language.json', 'pl');
        }
      });
  
      // Changes the site language and stores language preference to local storage
      function changeLanguage(lang) {
        $.MultiLanguage('language.json', lang);
        // Check if the localStorage object exists
        if (localStorage) {
          localStorage.setItem('user_lang', lang);
        }
      }

      // Cookie Consent box initialization
      window.cookieconsent.initialise({
        palette: {
          popup: {
            background: '#1a2c79',
          },
          button: {
            background: '#ffffff',
            text: '#1a2c79',
          },
        },
        theme: 'classic',
        position: 'bottom-left',
        content: {
          message:
            'This website uses cookies to ensure you get the best user experience.',
          href: 'privacypolicy.txt',
        },
      });

      // Slick slider initialization
      $(document).ready(function () {
        $('.autoplay').slick({
          slidesToShow: 5,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 2000,
          // conditional rendering based on screen size
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
              },
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
              },
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
              },
            },
          ],
        });
      });