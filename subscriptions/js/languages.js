// ========================================= LISTENER ==================================== //

$(document).ready(function () {
  var currentLanguage = 'english';

  // Hide all other languages when the web page loads
  $('.greek').hide();
  $('.polish').hide();

  // onclick behavior
  $('.lang').click(function () {
    var nextLanguage = $(this).attr('id'); // obtain language id

    if (nextLanguage != currentLanguage) {
      // find all content with current language under the div post-content and hide it
      $('.' + currentLanguage).each(function (i) {
        $(this).fadeToggle('slow', function () {
          // find all content with next language under the div post-content and show it
          $(this)
            .parent()
            .find('.' + nextLanguage)
            .show();
        });
      });
      // set as current language the next language
      currentLanguage = nextLanguage;
    }
  });
});
